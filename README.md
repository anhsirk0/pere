<h1 align="center"><code>Pere</code></h1>
<p align="center">Simple <strong>Bulk</strong> file renamer</p>

## About
Pere is a bulk file renamer written in Perl.

## Features
Pere can
 - rename files by pattern
 - add numbering to filenames
 - truncate filenames
 - delete chars from filenames
 - search and replace text from filenames
 - search and replace text from filenames
 - also rename suffix (file extensions)
 - revert the rename using logfile

## Installation
Its just a perl script
download it make it executable and put somewhere in your $PATH

## Manually
with wget
``` bash
wget https://raw.githubusercontent.com/anhsirk0/pere/master/pere.pl -O pere
```
### or
with curl
``` bash
curl https://raw.githubusercontent.com/anhsirk0/pere/master/pere.pl --output pere
```
making it executable
```bash
chmod +x pere
```
copying it to $PATH (~/.local/bin/ , this step is optional)
```bash
cp pere ~/.local/bin/
```

## Usage
Old name is available as **{old}** in --pattern string  
Number (while numbering) is available as **{num}** in --pattern string

#### Search and replace
```bash
$ pere files* --replace "file" "new_file"
  files9.txt -> newfile_9.txt
  files10.txt -> newfile_10.txt
  2 Files renamed
```

#### Numbering
```bash
$ pere files* --num 9 --pattern "new_file_{num}"
  files9.txt -> newfile_9.txt
  files10.txt -> newfile_10.txt
  files11.txt -> newfile_11.txt
  3 Files renamed
```

#### Numbering and using old name
```bash
$ pere files* --num 1 --pattern "{old}_renamed"
  files9.txt -> files9_renamed.txt
  files10.txt -> files10_renamed.txt
  files11.txt -> files11_renamed.txt
  3 Files renamed
```

#### Numbering from one number to another 
```bash
$ pere files* --num 9 10 --num-format "newfile_{num}"
  files9.txt -> newfile_9.txt
  files10.txt -> newfile_10.txt
  2 Files renamed
```
> This will rename only first 2 files  
> This can also work in reverse direction (--num 10 1)

#### Truncate
```bash
$ pere *.txt --truncate 5
  1long_name.txt -> 1long.txt
  2long_name.txt -> 2long.txt
  2 Files renamed
```

Truncating (from end)
```bash
$ pere *.txt --truncate -5
  long_name1.txt -> name1.txt
  long_name2.txt -> name2.txt
  2 Files renamed
```

#### Delete
```bash
$ pere *.txt --delete 5
  long_name1.txt -> name1.txt
  long_name2.txt -> name2.txt
  2 Files renamed
```

Deleting (from end)
```bash
$ pere *.txt --delete -5
  1long_name.txt -> 1long.txt
  2long_name.txt -> 2long.txt
  2 Files renamed
```

Renaming with extension
```bash
$ pere *.jpg --replace "jpg" "png" --extension
  jpg_img_1.jpg -> png_img_1.png
  jpg_img_2.jpg -> png_img_2.png
  2 Files renamed
```

#### Finding files with regex
```bash
$ pere -f 'files[0-9]+.*' --replace "file" "new_file"
  files9.txt -> newfile_9.txt
  files10.txt -> newfile_10.txt
  2 Files renamed
```

#### Reverting the rename via logfile
logfiles are saved in `~/.config/pere`
```bash
pere -rev "pere_logfile"
```
## Available options
```text
usage: pere [files*] [options]

    -n, --num=INT INT            Start and end (optional) numbers
    -nf, --num-format=STR        text format (required for numbering)
    -r, --replace=STR STR        Search and replace
    -t, --truncate=INT           Number of chars to keep (use negative value
                                              to keep chars from end. Ex: -t -6)
    -d, --delete=INT             Number of chars to delete (use negative value
                                              to delete chars from end. Ex: -d -6)
    -e, --extension              rename file along with extension
    -dry, --dry-run              show what will happen without renaming
    -rev, --revert=STR           revert the rename (require a logfile)
    -nl, --no-log                dont save log
    -h, --help                   show this help message
```
